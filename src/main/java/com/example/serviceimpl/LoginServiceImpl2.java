package com.example.serviceimpl;

import com.example.service.LoginService;

import net.sf.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.controllers.LoginController;
import com.example.repository.LoginRepository;

@Service
public class LoginServiceImpl2 implements UserDetailsService{
	private static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl2.class);
	@Autowired
	private LoginRepository LoginRepository;
	
	private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		List<Object[]> info = LoginRepository.findUsername(username);
		List<JSONObject> cont = new ArrayList<JSONObject>();
		JSONObject data = new JSONObject();
		JSONObject isi = new JSONObject();
		info.stream().forEach((column) -> {
			
			
//			logger.info("column2" + column[2]);
			data.put("username", column[0]);
			data.put("password", column[1]);
			cont.add(data);
		});
		logger.info("data = "+data.getString("password"));
//		logger.info("data = "+info.get());
		if (data.getString("username").equals(username)) {
			return new User(data.getString("username"), bCryptPasswordEncoder.encode(data.getString("password")),
					new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		
		
		
//		if(info == null)
//		{
//			throw new UsernameNotFoundException("User not found with username: " + username);
//		}
//		return new org.springframework.security.core.userdetails.User(username, "123",
//				new ArrayList<>());
	}

}
