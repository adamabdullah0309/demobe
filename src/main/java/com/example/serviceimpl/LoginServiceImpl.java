package com.example.serviceimpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.controllers.LoginController;
import com.example.entity.Login;
import com.example.repository.LoginRepository;
import com.example.service.LoginService;

import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityManager;
import javax.persistence.Query;

@Service
public class LoginServiceImpl implements LoginService {
	private static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);
	@Autowired
	private LoginRepository LoginRepository;
	
	@Autowired
    EntityManagerFactory emf;
	
	@Autowired
	EntityManager em;
	
	@Autowired
	CacheManager cacheManager;

	//ini contoh untuk join
	@Override
	public List<Object[]> tabelSemua(String username, String password) {
	   Optional<List<?>> info = LoginRepository.cari(username, password);
       if (!info.isEmpty() && info.get().size() > 0) {
//    	   return (List<Object[]>) new org.springframework.security.core.userdetails.User(username, password,
//   				new ArrayList<>()); 
           return (List<Object[]>) info.get(); //ini yang sebelumnya
       }
       return null;
		
//		EntityManager em = emf.createEntityManager();
//		Query query = em.createQuery("select username, password"
//				+ " from acc_user"
//				+ " ");
//		@SuppressWarnings("unchecked")
//		List<Login> list = (List<Login>)query.getResultList();
//		em.close();
//		// TODO Auto-generated method stub
//		return list;
	}

	@Override
	public List<Object[]> coba222(String username, String password) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery(""
				+ "SELECT acc_user.username, acc_user.password, acc_user.id_company, acc_user.id_apps, acc_user.id_acc,"
				+ " acc_user_info.nama, acc_user_info.id_jabatan,div.id_division, "
				+ " comp.name_company, "
				+ " apps.apps_name, "
				+ " acc.acc_name, "
				+ " jab.nama_jabatan,"
				+ " div.name_division"
				+ " FROM acc_user"
				+ " left join acc_user_info on acc_user.username = acc_user_info.username"
				+ " left join acc_company AS comp on comp.id_company = acc_user.id_company"
				+ " left join acc_apps_name AS apps on apps.id_apps = acc_user.id_apps"
				+ " left join acc_accounting_services AS acc on acc.id_acc = acc_user.id_acc"
				+ " left join acc_jabatan AS jab on jab.id_jabatan = acc_user_info.id_jabatan"
				+ " left join acc_division AS div on div.id_division = acc_user_info.id_division"
				+ " where acc_user.username = :username and acc_user.password = :password").setParameter("username", username).setParameter("password", password).getResultList();
	}

	@Override
	public List<Object[]> login(String username, String password) {
		// TODO Auto-generated method stub
				return this.em.createNativeQuery(""
						+ "SELECT acc_user.username, acc_user.password, acc_user.id_company, acc_user.id_apps, acc_user.id_acc,"
						+ " acc_user_info.nama, acc_user_info.id_jabatan,div.id_division, "
						+ " comp.name_company, "
						+ " apps.apps_name, "
						+ " acc.acc_name, "
						+ " jab.nama_jabatan,"
						+ " div.name_division, "
						+ " acc_user.session_id,"
						+ " acc_user_info.kdkaryawan,"
						+ " p.kdjabatan,"
						+ " p.kddivisi,"
						+ " m_departement_id,"
						+ " o.nmjabatan,"
						+ " u.nmdivisi,"
						+ " x.departement_nama "
						+ " FROM acc_user"
						+ " left join acc_user_info on acc_user.username = acc_user_info.username"
						+ " left join acc_company AS comp on comp.id_company = acc_user.id_company"
						+ " left join acc_apps_name AS apps on apps.id_apps = acc_user.id_apps"
						+ " left join acc_accounting_services AS acc on acc.id_acc = acc_user.id_acc"
						+ " left join acc_jabatan AS jab on jab.id_jabatan = acc_user_info.id_jabatan"
						+ " left join acc_division AS div on div.id_division = acc_user_info.id_division "
						+ " left join tm_master_karyawan as p on acc_user.id_company = p.company_id and acc_user_info.kdkaryawan = p.kdkaryawan "
						+ " left join tm_master_jabatan as o on p.kdjabatan = o.kdjabatan and p.company_id = o.company_id\r\n" + 
						"	left join tm_master_divisi as u on p.kddivisi = u.kddivisi and p.company_id = u.company_id "
						+ " left join m_departement as x on x.departement_id = p.m_departement_id and x.company_id = acc_user.id_company "
						+ " where acc_user.username = :username and acc_user.password = :password").setParameter("username", username).setParameter("password", password).getResultList();
	}
	
	@Override
	public List<Object[]> login2(String username, String password) {
		// TODO Auto-generated method stub
				return this.em.createNativeQuery(""
						+ "SELECT acc_user.username, acc_user.password, acc_user.id_company, acc_user.id_apps, acc_user.id_acc,"
						+ " acc_user_info.nama, acc_user_info.id_jabatan,div.id_division, "
						+ " comp.name_company, "
						+ " apps.apps_name, "
						+ " acc.acc_name, "
						+ " jab.nama_jabatan,"
						+ " div.name_division,"
						+ " acc_user_info.no_telp," //14
						+ " acc_user.session_id," //15
						+ " acc_user.otp" //16
						+ " FROM acc_user"
						+ " left join acc_user_info on acc_user.username = acc_user_info.username"
						+ " left join acc_company AS comp on comp.id_company = acc_user.id_company"
						+ " left join acc_apps_name AS apps on apps.id_apps = acc_user.id_apps"
						+ " left join acc_accounting_services AS acc on acc.id_acc = acc_user.id_acc"
						+ " left join acc_jabatan AS jab on jab.id_jabatan = acc_user_info.id_jabatan"
						+ " left join acc_division AS div on div.id_division = acc_user_info.id_division"
						+ " where acc_user.username = :username and acc_user.password = :password").setParameter("username", username).setParameter("password", password).getResultList();
	}

	@Override
	public UserDetails tabelSemua1(String username, String password) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		Optional<List<?>> info = LoginRepository.cari(username, password);
		if(info.isEmpty())
		{
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		return new org.springframework.security.core.userdetails.User(username, password,
				new ArrayList<>());
		
	}

	@Override
	public UserDetails loadUserByUsername(String username, String password) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
//		Optional<List<?>> info = LoginRepository.cari(username, password);
		
		Optional<List<?>> info = LoginRepository.cari(username, password);
		if(info == null)
		{
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		return new org.springframework.security.core.userdetails.User(username, password,
				new ArrayList<>());
//		
//		if(info.isEmpty())
//		{
//			throw new UsernameNotFoundException("User not found with username: " + username);
//		}
//		return new org.springframework.security.core.userdetails.User(username, password,
//				new ArrayList<>());
//		logger.info("info = "+ info.get().size());
//		logger.info("info" + info);
//		if(info.isEmpty())
//		{
//			throw new UsernameNotFoundException("User not found with username: " + username);
//		}
//		else
//		{
//			return new User(username, "$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6",
//					new ArrayList<>());
//		}
		
//		if ("javainuse".equals(username)) {
//			return new User(username, "$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6",
//					new ArrayList<>());
//		} else {
//			throw new UsernameNotFoundException("User not found with username: " + username);
//		}
		
	}

	@Override
	public void evictAllCaches() {
		// TODO Auto-generated method stub
		 cacheManager.getCacheNames().stream()
	      .forEach(cacheName -> cacheManager.getCache(cacheName).clear());
	}

}
