package com.example.entity;

public class AuthToken {
	private String token;
    private String username;
    private String password;
    private int companyId;
    private String companyName;
    private int AppsId;
    private String appsName;
    private int accId;
    private String accName;
    private int titleId;
    private String titleName;
    private int divisionId;
    private String divisionName;
    private String fullname;
    private String session;
    private String kdkaryawan;
    private String kdjabatan;
    private String kddivisi;
    private String departementId;
    private String nmjabatan;
    private String nmdivisi;
    private String departementNama;
    public AuthToken(){

    }

    public AuthToken(String token, String username, String password, int companyId, String companyName, int AppsId, String appsName, int accId, String accName, int titleId,String titleName,int divisionId,String divisionName,String fullname, String session,String kdkaryawan,
    		String kdjabatan, String kddivisi, String departementId,String nmjabatan, String nmdivisi, String departementNama){
        this.token = token;
        this.username = username;
        this.password = password;
        this.companyId = companyId;
        this.companyName = companyName;
        this.AppsId = AppsId;
        this.appsName = appsName;
        this.accId = accId;
        this.accName = accName;
        this.titleId = titleId;
        this.titleName = titleName;
        this.divisionId = divisionId;
        this.divisionName = divisionName;
        this.fullname = fullname;
        this.session = session;
        this.kdkaryawan = kdkaryawan;
        this.kdjabatan = kdjabatan;
        this.kddivisi = kddivisi;
        this.departementId = departementId;
        this.nmjabatan = nmjabatan;
        this.nmdivisi = nmdivisi;
        this.departementNama = departementNama;
    }
    
    

    public String getNmjabatan() {
		return nmjabatan;
	}

	public void setNmjabatan(String nmjabatan) {
		this.nmjabatan = nmjabatan;
	}

	public String getNmdivisi() {
		return nmdivisi;
	}

	public void setNmdivisi(String nmdivisi) {
		this.nmdivisi = nmdivisi;
	}

	public String getDepartementNama() {
		return departementNama;
	}

	public void setDepartementNama(String departementNama) {
		this.departementNama = departementNama;
	}

	public String getKdjabatan() {
		return kdjabatan;
	}

	public void setKdjabatan(String kdjabatan) {
		this.kdjabatan = kdjabatan;
	}

	public String getKddivisi() {
		return kddivisi;
	}

	public void setKddivisi(String kddivisi) {
		this.kddivisi = kddivisi;
	}

	public String getDepartementId() {
		return departementId;
	}

	public void setDepartementId(String departementId) {
		this.departementId = departementId;
	}

	public String getKdkaryawan() {
		return kdkaryawan;
	}

	public void setKdkaryawan(String kdkaryawan) {
		this.kdkaryawan = kdkaryawan;
	}

	public AuthToken(String token){
        this.token = token;
    }

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getAppsId() {
		return AppsId;
	}

	public void setAppsId(int appsId) {
		AppsId = appsId;
	}

	public String getAppsName() {
		return appsName;
	}

	public void setAppsName(String appsName) {
		this.appsName = appsName;
	}

	public int getAccId() {
		return accId;
	}

	public void setAccId(int accId) {
		this.accId = accId;
	}

	public String getAccName() {
		return accName;
	}

	public void setAccName(String accName) {
		this.accName = accName;
	}

	public int getTitleId() {
		return titleId;
	}

	public void setTitleId(int titleId) {
		this.titleId = titleId;
	}

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public int getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(int divisionId) {
		this.divisionId = divisionId;
	}

	public String getDivisionName() {
		return divisionName;
	}

	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

    
}
