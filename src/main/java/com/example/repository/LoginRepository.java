package com.example.repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.entity.Login;


public interface LoginRepository extends JpaRepository<Login, String> {
//	public Optional<Login> findByUsernameAndPassword(final String username, final String password);
	
//	@Query(value = "SELECT users.username','users.password',\r\n" + 
//			"			'users.id_company','comp.name_company',\r\n" + 
//			"			'users.id_apps','apps.apps_name',\r\n" + 
//			"			'users.id_acc','acc.acc_name',\r\n" + 
//			"			'userinfo.nama','userinfo.id_jabatan',\r\n" + 
//			"			'jab.nama_jabatan','userinfo.id_division',\r\n" + 
//			"			'div.name_division FROM acc_user AS users "
//			+ "join acc_user_info AS userinfo on users.username = userinfo.username"
//			+ "join acc_company AS comp on comp.id_company = users.id_company"
//			+ "join acc_apps_name AS apps on apps.id_apps = users.id_apps"
//			+ "join acc_accounting_services AS acc on acc.id_acc = users.id_acc"
//			+ "join acc_jabatan AS jab on jab.id_jabatan = userinfo.id_jabatan"
//			+ "join acc_division AS div on div.id_division = userinfo.id_division "
//			+ "where users.username =?1 and user.password = ?2",nativeQuery = true)
	
	@Query(value="select acc_user.username, acc_user.password, acc_user_info.nama, acc_user_info.id_jabatan"
			+ " from acc_user"
			+ " left join acc_user_info on acc_user.username = acc_user_info.username"
			+ " where acc_user.username = ?1 and acc_user.password = ?2", nativeQuery=true)
	
//	String query ="select t_pendaftaran.no_registrasi, m_unit.unit_nama "
//			+ "from t_pendaftaran "
//			+ "left join m_unit on m_unit.unit_id = t_pendaftaran.m_unit_id";
	Optional<?> findAll(final String username, final String password);
	
	//ini contoh untuk join
	  @Query(value="SELECT acc_user.username, acc_user_info.no_telp, acc_user.password "
	  		+ " FROM acc_user"
	  		+ " left join acc_user_info on acc_user.username = acc_user_info.username"
	  		+ " where acc_user.username = ?1 and acc_user.password = ?2", nativeQuery = true)
	   Optional<List<?>> cari(final String username, final String password);
	  
	  @Query(value="SELECT acc_user.username, acc_user.password, acc_user.id_company, acc_user.id_apps, acc_user.id_acc,"
				+ " acc_user_info.nama, acc_user_info.id_jabatan,div.id_division, "
				+ " comp.name_company, "
				+ " apps.apps_name, "
				+ " acc.acc_name, "
				+ " jab.nama_jabatan,"
				+ " div.name_division"
				+ " FROM acc_user"
				+ " left join acc_user_info on acc_user.username = acc_user_info.username"
				+ " left join acc_company AS comp on comp.id_company = acc_user.id_company"
				+ " left join acc_apps_name AS apps on apps.id_apps = acc_user.id_apps"
				+ " left join acc_accounting_services AS acc on acc.id_acc = acc_user.id_acc"
				+ " left join acc_jabatan AS jab on jab.id_jabatan = acc_user_info.id_jabatan"
				+ " left join acc_division AS div on div.id_division = acc_user_info.id_division"
		  		+ " where acc_user.username = ?1", nativeQuery = true)
	  List<Object[]> findUsername(final String username);
	   
	  
	
	
}
