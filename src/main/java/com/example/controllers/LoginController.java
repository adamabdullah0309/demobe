package com.example.controllers;

import java.util.ArrayList;
import java.util.List;
import org.springframework.security.authentication.AuthenticationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.*;
import com.example.config.*;
import com.example.service.LoginService;
import com.example.config.JwtTokenUtil;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.springframework.security.core.userdetails.UserDetailsService;

@RestController
@RequestMapping("/api")
public class LoginController {
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private UserDetailsService jwtInMemoryUserDetailsService;
	
	@Autowired
	private LoginService LoginService;
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@CrossOrigin
	public ApiResponse<AuthToken> register (@RequestBody JwtRequest authenticationRequest) throws Exception {
		logger.info("loggin ");
		JSONObject response = new JSONObject();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(authenticationRequest);
		List<JSONObject> cont = new ArrayList<JSONObject>();
//		try
//		{
		
		
		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
		final UserDetails userDetails = jwtInMemoryUserDetailsService
				.loadUserByUsername(authenticationRequest.getUsername());
		
		final String token = jwtTokenUtil.generateToken(userDetails);
//		  final User user = userService.findOne(loginUser.getUsername());
		List<Object[]> list = LoginService.login(dataRequest.getString("username"),dataRequest.getString("password"));
		
		
		JSONObject data = new JSONObject();
		JSONObject isi = new JSONObject();
		list.stream().forEach((column) -> {
//			column[8] == null ? "" : column[8].toString()
			
			data.put("username", column[0] == null || column[0] == "" ? "" : column[0]);
			data.put("password", column[1] == null || column[1] == "" ? "" : column[1]);
			data.put("companyId", column[2] == null || column[2] == "" ? "" : column[2]);
			data.put("companyName",column[8] == null || column[8] == "" ? "" : column[8]);
			data.put("AppsId", column[3] == null || column[3] == "" ? "" : column[3]);
			data.put("appsName", column[9] == null || column[9] == "" ? "" : column[9]);
			data.put("accId", column[4] == null || column[4] == "" ? "" : column[4]);
			data.put("accName", column[10] == null || column[10] == "" ? "" : column[0]);
			
			data.put("titleId", column[6] == null || column[6] == "" ? "" : column[6]);
			data.put("titleName", column[11] == null || column[11] == ""? "" : column[11]);
			data.put("divisionId", column[7] == null || column[7] == "" ? "" : column[7]);
			data.put("divisionName", column[12] == null || column[12] == "" ? "" : column[12]);
			data.put("fullname", column[5] == null || column[5] == "" ? ""  : column[5]);
			data.put("session", column[13] == null || column[13] == ""? "" : column[13]);
			data.put("kdkaryawan", column[14] == null || column[14] == ""? "" : column[14]);
			data.put("kdjabatan", column[15] == null || column[15] == ""? "" : column[15]);
			data.put("kddivisi", column[16] == null || column[16] == ""? "" : column[16]);
			data.put("departementId", column[17] == null || column[17] == ""? "" : column[17]);
			data.put("nmjabatan", column[18] == null || column[18] == ""? "" : column[18]);
			data.put("nmdivisi", column[19] == null || column[19] == ""? "" : column[19]);
			data.put("departementNama", column[20] == null || column[20] == ""? "" : column[20]);
			cont.add(data);
		});
		response.put("responseCode", "00");
		response.put("responseDesc", "Login Success");
		response.put("responseData", data);
		response.put("token", ResponseEntity.ok(new JwtResponse(token)).getBody().getToken().toString());
		return new ApiResponse<>(200, "success",new AuthToken(token, 
				cont.get(0).getString("username"),
				cont.get(0).getString("password"),
				cont.get(0).getInt("companyId"),
				cont.get(0).getString("companyName"),
				cont.get(0).getInt("AppsId"),
				cont.get(0).getString("appsName"),
				cont.get(0).getInt("accId"),
				cont.get(0).getString("accName"),
				cont.get(0).getInt("titleId"),
				cont.get(0).getString("titleName"),
				cont.get(0).getInt("divisionId"),
				cont.get(0).getString("divisionName"),
				cont.get(0).getString("fullname"),
				cont.get(0).getString("session"),
				cont.get(0).getString("kdkaryawan"),
				cont.get(0).getString("kdjabatan"),
				cont.get(0).getString("kddivisi"),
				cont.get(0).getString("departementId"),
				cont.get(0).getString("nmjabatan"),
				cont.get(0).getString("nmdivisi"),
				cont.get(0).getString("departementNama")
				));
//		}
//		catch(Exception e)
//		{
//			response.put("responseCode", "99");
//			response.put("responseDesc", "Login Failed !");
//			response.put("responseError", e.getMessage());
//			JSONObject data = new JSONObject();
//			data.put("username", "");
//			data.put("password", "");
//			data.put("companyId", 0);
//			data.put("companyName","");
//			data.put("AppsId", 0);
//			data.put("appsName", "");
//			data.put("accId", 0);
//			data.put("accName", "");
//			
//			data.put("titleId", 0);
//			data.put("titleName", "");
//			data.put("divisionId", 0);
//			data.put("divisionName", "");
//			data.put("fullname", "");
//			data.put("session", "");
//			cont.add(data);
//			response.put("token","");
//		}
//		int status = Integer.parseInt(response.getString("responseCode"));
		
		//---------------------------------------------ini yang untuk return seluruh salah dan benar
//		return new ApiResponse<>(response.getString("responseCode"), response.getString("responseDesc"), new AuthToken(
//				response.getString("token").isEmpty() ? "" : response.getString("token"),				
//				cont.get(0).getString("username").isBlank() ? "" : cont.get(0).getString("username"),
//				cont.get(0).getString("password").isBlank() ? "" : cont.get(0).getString("password"),
//
//						
//				cont.get(0).getInt("companyId"),
//				cont.get(0).getString("companyName").isBlank() ? "" : cont.get(0).getString("companyName"),
//				
//				cont.get(0).getInt("AppsId"),
//				cont.get(0).getString("appsName").isBlank() ? "" : cont.get(0).getString("companyName"),
//						
//				cont.get(0).getInt("accId"),
//				cont.get(0).getString("accName").isBlank() ? "" : cont.get(0).getString("companyName"),
//				
//				cont.get(0).getInt("titleId"),
//				cont.get(0).getString("titleName").isBlank() ? "" : cont.get(0).getString("titleName"),
//				
//				cont.get(0).getInt("divisionId"),
//				cont.get(0).getString("divisionName").isBlank() ? "" : cont.get(0).getString("titleName"),
//				cont.get(0).getString("fullname").isBlank() ? "" : cont.get(0).getString("titleName"),
//				cont.get(0).getString("session").isBlank() ? "" : cont.get(0).getString("titleName") ));
		//---------------------------------------------------------------------------------------------------------
		
//		return response.toString();
//		return new AuthResponse<>(200, "success",new AuthToken(token, user.getUsername(), user.getId(), 	company.getUserCompany(), contentCompany.getCompanyName(), contentCompany.getCompanyLogo(), session, 	user.getTelpUser(), user.getEmail()));

//		return ResponseEntity.ok(new JwtResponse(token));
		
//			authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
//			final UserDetails userDetails = LoginService.tabelSemua1(authenticationRequest.getUsername(),authenticationRequest.getPassword());
//			final String token = jwtTokenUtil.generateToken(userDetails);
//			
//			response.put("responseCode", "00");
//			response.put("responseDesc", "Login Success");
//			response.put("token", );
//			return ResponseEntity.ok(new JwtResponse(token));
			
			
//			logger.info("username "+userDetails);
//		}
//		catch(Exception e)
//		{
//			response.put("responseCode", "99");
//			response.put("responseDesc", "Logout failed");
//			response.put("responseError", "Username for login not found!");
//		}
//		return response.toString();
	}
	
	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS"+ e.getMessage());
		}
	}
}	
