package com.example.service;


import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.example.entity.Login;


@Component
public interface LoginService {
	
	//ini contoh untuk join
	public List<Object[]> tabelSemua(String username, String password);
	
	UserDetails loadUserByUsername(String username, String password) throws UsernameNotFoundException;
	
	public List<Object[]> login(String username, String password);
	public List<Object[]> login2(String username, String password);
	
	public UserDetails tabelSemua1(String username, String password);
	
	//ini contoh join dari mbk intan 
	public List<Object[]> coba222(String username, String password);
	public void evictAllCaches();
}
